package com.classpath.empcrud.controller;

import com.classpath.empcrud.model.Tweet;
import com.classpath.empcrud.model.User;
import com.classpath.empcrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import  java.util.List;
import java.util.Set;

@Controller
public class UserWebController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String displayHomePage(){
        return "welcome";
    }

    @GetMapping("/users")
    public String listUsers(Model model){
        List<User> listOfUsers = this.userService.getAllUsers();

        model.addAttribute("users", listOfUsers);
        return "list";
    }

    @GetMapping("/register")
    public String registrationPage(Model model){
        model.addAttribute("user", new User());
        return "register";
    }
    @GetMapping("/upload")
    public String fileUpload(){
        return "upload";
    }

    @PostMapping("/submit")
    public String registerUser( @Valid @ModelAttribute("user") User user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "register";
        }
        this.userService.saveUser(user);
        return "redirect:/users";// redirect the url
        //dont use the redirecr: forwarded and the url will not change
    }

    @GetMapping("/users/{id}/tweets")
    public String getAllTweetsByUserId(@PathVariable("id") long userId, Model model){
        Set<Tweet> tweets = this.userService.getAllTweetsByUserId(userId);
        model.addAttribute("tweets", tweets);
        return "tweets";
    }

   @PostMapping(value = "/uploadFile")
    public @ResponseBody
    String uploadFileHandler(@RequestParam("name") String name,
                             @RequestParam("file") MultipartFile file) {

        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();

                // Creating the directory to store file
                String rootPath = System.getProperty("catalina.home");
                File dir = new File(rootPath + File.separator + "tmpFiles");
                if (!dir.exists())
                    dir.mkdirs();

                // Create the file on server
                File serverFile = new File(dir.getAbsolutePath()
                        + File.separator + name);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();

                return "You successfully uploaded file=" + name;
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "success";
        }
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleControllerException(Model model, Exception e){
        model.addAttribute("message", e.getMessage());
        return "error";
    }

}